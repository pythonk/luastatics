// ==UserScript==
// @name        jsKillAD
// @namespace   jsKillAD
// @include     http://*
// @include     https://*
// @grant       none
// @version     1
// ==/UserScript==
(function () {
	var bc = Array.prototype.forEach;//Callback
	function getStyle(o, s) {
		if(o.style[s])//内联样式
			return o.style[s];
		else if(document.defaultView && document.defaultView.getComputedStyle){//DOM
			//s = s.replace(/([A-Z])/g,'-$1').toLowerCase();
			var x = document.defaultView.getComputedStyle(o,'');
			return x.getPropertyValue(s);
		}
	}

	function testStyle(o) {
		var s = getStyle(o, "position");
		return s === "fixed" || s === "absolute";
	}
	function isFloatLay(o) {
		var x = o.offsetParent;
		return !x || x.tagName === "BODY" || x.tagName === "HTML";
	}

	function delNode(x) {
		x.parentNode.removeChild(x);
	}

	function scan(el) {
		["iframe", "img", "object", "embed", "video"].forEach(function(s) {
			bc.call(el.getElementsByTagName(s), function(x) {
				while (x) {
					if (isFloatLay(x)) {
						//要删除的层得同时满足二个条件
						if (testStyle(x)) delNode(x);
						break;
					}
					x = x.offsetParent;
				}
			});
		});
	}
	document.addEventListener("DOMContentLoaded", function(){scan(document);}, false);
	bc.call(frames, function(x) {
		try {
			scan(x.contentDocument);
		} catch (e) {}
	});
})();

var interval = 3000;
(function () {
  //js脚本屏蔽网页元素
  var removeD = n => {
      n.split(",").forEach(v => {
          if (v.indexOf("@ID(") == 0) {
              document.getElementById(v.substring(4, v.length - 1)).style.display = "none";
          } else {
              //for (let e of document.getElementsByClassName(v)) e.style.display = "none";
              var x = document.getElementsByClassName(v);
              var i;
              for (i = 0; i < x.length; i++) {
                  x[i].style.display = "none";
              }
          }
      })
  }
  var currentURL = window.location.href;
  var shoupa = /shoupa/;
  if(shoupa.test(currentURL)){
      setTimeout(function () {
          //document.getElementsByClassName("rank-group")[0].remove();
          removeD("navbar-brand,copyright,friend-link,comments-user,comments-group,footer");   
      }, interval);
  }
  if(location.href.match(".shoupa.com") ||location.href.match(".shoupp.cn")) {
    document.getElementsByClassName("navbar-brand")[0].remove();
    removeD("navbar-brand,copyright,friend-link,comments-user,comments-group,footer");     
  }

})();